import React, { Component } from 'react';
import Title from '../title';
import AddTask from '../add-task';

import './main.css'

class Main extends Component  { 
    state = {taskArr:[]}
 onUpDateTaskArr = (object) => {
     this.setState(
        ({taskArr}) => { //тіло фуnкції
        const newArr = taskArr;
          newArr.push(object)
          return {
              taskArr: newArr
        }
        }
     ) 
 }
    render () {
        const {taskArr} = this.state;
        console.log(taskArr);
    return (
        <div className='main_div'>
            <Title text='Список задач на сегодня'> </Title>
            <AddTask taskArr={taskArr} onUpDateTaskArr = {this.onUpDateTaskArr}></AddTask>
            <Title text='Осталось __ задач'> </Title>
        </div>
    )
}
}
export default Main;
