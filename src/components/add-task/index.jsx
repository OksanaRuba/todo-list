import React from 'react';
import Input from '../input';
import TaskList from '../task-list';


export default function AddTask({onUpDateTaskArr,taskArr}){
    return(
        <div>
            <Input onUpDateTaskArr = {onUpDateTaskArr}/>
            <TaskList taskArr={taskArr}></TaskList>
        </div>
    )
} 