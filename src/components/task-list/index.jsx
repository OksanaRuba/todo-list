import React from 'react';

import './task-list.css';

const TaskList = ({ taskArr }) => {
    return (
        <div>
            <ul>
                {taskArr.map((task, index) => {
                    return (
                        <li className="task-list" key={index * 5 + 'b'}>{task.text},{task.date.getDate()}
                            <div><i className="fas fa-pen"></i></div>
                            <div><i className="far fa-trash-alt"></i></div>
                        </li>)
                })}
            </ul>
        </div>
    )
}

export default TaskList;